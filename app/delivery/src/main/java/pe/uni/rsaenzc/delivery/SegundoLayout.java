package pe.uni.rsaenzc.delivery;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class SegundoLayout extends AppCompatActivity {

    Spinner spinnerCantidad;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioGroup radioGroup;
    Button button;
    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.segundo_layout);

        spinnerCantidad = findViewById(R.id.spinner);
        button = findViewById(R.id.button);
        radioButton1 = findViewById(R.id.radio_button_logo_1);
        radioButton2 = findViewById(R.id.radio_button_logo_2);
        radioGroup = findViewById(R.id.radio_group);

        adapter = ArrayAdapter.createFromResource(this, R.array.total, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerCantidad.setAdapter(adapter);
    }
}
