package pe.uni.rsaenzc.delivery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class DeliveryActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<String> text1 = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        PrimerLayout gridAdapter = new PrimerLayout(this,text,text1,image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG).show();

            Intent intent = new Intent(DeliveryActivity.this, SegundoLayout.class);
            startActivity(intent);

        });

    }

    private void fillArray(){
        text.add("Bisteck a lo pobre");
        text.add("Caldo de gallina");
        text.add("Caldo de menudencia");
        text.add("Camarones con papas fritas");
        text.add("Ceviche");
        text.add("Ensalada de fruta");
        text.add("Frejoles con milanesa");
        text.add("Salchipapa brava");

        text1.add("Bisteck a la parrilla acompañado de arroz");
        text1.add("Caldo de gallina con cebolla china y papa");
        text1.add("Caldo de menundencia con queso y cancha");
        text1.add("Camarones fritos con papa fritas y arroz");
        text1.add("Ceviche de bonito con camote y cancha");
        text1.add("Ensalada de manzana, ciruelas y platano");
        text1.add("Frejoles consasa milanesa con arroz");
        text1.add("Salchipapa con chorizo, hot dog y pollo");

        image.add(R.drawable.a);
        image.add(R.drawable.b);
        image.add(R.drawable.c);
        image.add(R.drawable.d);
        image.add(R.drawable.e);
        image.add(R.drawable.f);
        image.add(R.drawable.g);
        image.add(R.drawable.h);
    }


}