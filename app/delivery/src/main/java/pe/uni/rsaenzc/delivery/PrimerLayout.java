package pe.uni.rsaenzc.delivery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PrimerLayout extends BaseAdapter {
    Context context;
    ArrayList<String> text;
    ArrayList<String> text1;
    ArrayList<Integer> image;

    public PrimerLayout(Context context, ArrayList<String> text, ArrayList<String> text1, ArrayList<Integer> image){
        this.context = context;
        this.text = text;
        this.text1 = text1;
        this.image = image;
    }

    @Override
    public int getCount() {
        return text.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.primer_layout,parent,false);
        TextView textView1 = view.findViewById(R.id.text_view1);
        ImageView imageView = view.findViewById(R.id.image_view_comida);
        TextView textView = view.findViewById(R.id.text_view2);
        textView1.setText(text.get(position));
        imageView.setImageResource(image.get(position));
        textView.setText(text1.get(position));
        return view;
    }
}
